/* eslint-disable no-restricted-globals */

const CACHE_NAME = "PWA_OFFLINE-v0.0.1";

// no need for this now
// eslint-disable-next-line no-unused-vars
const CACHED_RESOURCES = [
  "/",
  "index.html",
  "logo192.png",
  "logo512.png",
  "/static/css",
  "/static/media",
  "/static/js",
];

const swLog = (message) => console.log(`Service Worker: ${message}`);

self.addEventListener("install", (e) => {
  swLog("installed");

  // cache resources statically
  // e.waitUntil(
  //   caches.open(CACHE_NAME).then((cache) => {
  //     cache.addAll(CACHED_RESOURCES);
  //   })
  // );
});

self.addEventListener("activate", (e) => {
  swLog("activated");

  e.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.map((cache) => {
          if (cache !== CACHE_NAME) {
            swLog("removed cache");
            return caches.delete(cache);
          }
        })
      );
    })
  );
});

self.addEventListener("fetch", (e) => {
  e.respondWith(
    fetch(e.request)
      .then((res) => {
        const resCopy = res.clone();

        caches.open(CACHE_NAME).then((cache) => {
          cache.put(e.request, resCopy);
        });

        return res;
      })
      .catch(() => caches.match(e.request))
  );
});

// youzles
self.addEventListener("push", (e) => {
  console.log("pushed from main process: ", e.data.text());
});
