export function register() {
  if ("serviceWorker" in navigator) {
    const url = `${process.env.PUBLIC_URL}/sw.js`;

    navigator.serviceWorker.register(url);
  } else {
    console.log("Service Workers Are Not Supported");
  }
}
