import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";

const DEFAULT_STATE = {
  online: navigator.onLine,
};

const ConnectivityContext = createContext(DEFAULT_STATE);

export const useConnectivity = () => {
  const context = useContext(ConnectivityContext);

  if (!context) throw Error("Context unreachable lol");

  return context;
};

export const ConnectivityProvider = ({ children }) => {
  const [state, setState] = useState(DEFAULT_STATE);

  const contextValue = {
    online: state.online,
  };

  const handleOffline = useCallback(() => {
    setState({ online: false });
  }, [setState]);

  const handleOnline = useCallback(() => {
    setState({ online: true });
  }, [setState]);

  useEffect(() => {
    window.addEventListener("offline", handleOffline);
    window.addEventListener("online", handleOnline);
    return () => {
      window.removeEventListener("offline", handleOffline);
      window.removeEventListener("online", handleOnline);
    };
  }, [handleOnline, handleOffline]);

  return (
    <ConnectivityContext.Provider value={contextValue}>
      {children}
    </ConnectivityContext.Provider>
  );
};
