import React from "react";
import { useConnectivity } from "./ConnectivityContext";

const ConnectivityBanner = () => {
  const { online } = useConnectivity();

  return (
    <div className={`connectivity-banner ${online ? "online" : "offline"}`}>
      {online ? "We're Online 🎉🎉" : "We're Offline 🧐"}
    </div>
  );
};

export default ConnectivityBanner;
