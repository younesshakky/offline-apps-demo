import logo from "./logo.svg";
import "./App.css";
import { ConnectivityProvider } from "./ConnectivityContext";
import ConnectivityBanner from "./ConnectivityBanner";

function App() {
  return (
    <ConnectivityProvider>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2 style={{ marginBottom: 4 }}>
            Introduction to offline web applications
          </h2>
          <p style={{ fontSize: "14px" }}>A humble demo</p>

          <ConnectivityBanner />
        </header>
      </div>
    </ConnectivityProvider>
  );
}

export default App;
